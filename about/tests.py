from django.test import TestCase
from django.urls import resolve
from . import views
from django.http import HttpRequest

class AboutTest(TestCase):
    def test_about_url_exist(self):
        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)
    
    def test_about_views_using_index_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, views.about)
    
    def test_about_using_about_template(self):
        response = self.client.get('/about/')
        self.assertTemplateUsed(response, 'about.html')
    

