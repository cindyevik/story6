from django.test import TestCase
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.contrib.auth import login
from django.http import HttpRequest

# Create your tests here.
class LoginTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            'cindy', 'cindy@abc.com', 'cindy'
        )
        
    def test_loginpage_exists(self):
        response = self.client.get('/loginpage/')
        self.assertEqual(response.status_code, 200)
    
    def test_loginpage_not_logged_in(self):
        response = self.client.get('/loginpage/')
        html = response.content.decode()
        self.assertIn('You are not logged in.', html)
        self.assertNotIn(self.user.username, html)
    
    def test_login_exists(self):
        response = self.client.get('/loginpage/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_login_logged_in_redirect(self):
        self.client.login(username='cindy', password='cindy')
        response = self.client.get('/loginpage/login/')
        self.assertEqual(response.status_code, 302)

    def test_logout_exists(self):
        response = self.client.get('/loginpage/logout/')
        self.assertEqual(response.status_code, 302)
