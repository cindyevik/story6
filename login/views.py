from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.contrib.auth import authenticate 
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from datetime import datetime


# Create your views here.
def index(request) :
    return render(request, 'index.html')

def login(request):
    if request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(
                request, username=username, password=password
            )
            request.session['username'] = user.username
            request.session['password'] = user.password
            if user is not None:
                auth_login(request, user)
                return redirect('index')
            else:
                pass
    return render(request, 'login.html')

def logout(request):
    auth_logout(request)
    return redirect('index')