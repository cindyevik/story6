from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from . import views


# Create your tests here.
class BookTest(TestCase):
    def test_book_url_exist(self):
        response = self.client.get('/book-finding/')
        self.assertEqual(response.status_code, 200)

    def test_book_views_function(self):
        found = resolve('/book-finding/')
        self.assertEqual(found.func, views.book)

    def test_status_using_book_template(self):
        response = self.client.get('/book-finding/')
        self.assertTemplateUsed(response, 'book.html')