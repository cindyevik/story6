from django.db import models
import datetime

# Create your models here.
class Status(models.Model):
	status = models.TextField(max_length=300)
	dates = models.DateTimeField(auto_now_add=True)