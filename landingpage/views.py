from django.shortcuts import render
from .forms import StatusForm
from landingpage.models import Status
from django.shortcuts import redirect
# Create your views here.
def status_form(request):
	status = Status.objects.all()
	status_form = StatusForm()
	if request.method == "POST":
		status_form = StatusForm(request.POST)
		if status_form.is_valid():
			status_form.save()
			return redirect('home')

	context =  {
		'form' : status_form, 
		'obj' : status
	}
	return render(request, 'landingpage.html', context)