from django.test import TestCase
from django.urls import resolve
from . import views
from django.http import HttpRequest
from .models import Status
from . import forms

# Create your tests here.
class StatusTest(TestCase):
    def test_status_url_exist(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_status_views_function(self):
        found = resolve('/')
        self.assertEqual(found.func, views.status_form)
    
    def test_status_model_create_new_status(self):
        Status.objects.create(status = 'baik')
        hitungjumlah = Status.objects.all().count()
        self.assertEqual(hitungjumlah, 1)
    
    def test_save_POST_request(self):
        response = self.client.post('', data={'status' : 'Tidak baik'})
        hitungjumlah = Status.objects.all().count()
        self.assertEqual(hitungjumlah, 1)
        self.assertEqual(response.status_code, 302)
        new_response = self.client.get('')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Tidak baik', html_response)

    def test_status_view(self):
        request = HttpRequest()
        response = views.status_form(request)
        self.assertContains(response, '')
    
    def test_status_using_landingpage_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'landingpage.html')

    def test_forms_valid(self):
        form_data = {'status' : 'helo'}
        form = forms.StatusForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_forms_not_valid(self):
        form_data = {'status' : 'a'*301}
        form = forms.StatusForm(data=form_data)
        self.assertFalse(form.is_valid())
    

    
