from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time

class Story6FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        return super(Story6FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.browser.close()
        return super(Story6FunctionalTest, self).tearDown()

    def test_h1_is_exist(self):
       ## self.browser.get('https://cindy-story6.herokuapp.com')
        self.browser.get(self.live_server_url)
        h = self.browser.find_element_by_id('welcome_text')
        self.assertEqual(h.text,'Halo, apa kabar?')
    
    def test_input(self):
        self.browser.get(self.live_server_url)
        status = self.browser.find_element_by_tag_name('textarea')
        submit = self.browser.find_element_by_id('button2')
        status.send_keys('keren')
        submit.send_keys(Keys.RETURN)
        self.assertIn('keren', self.browser.page_source)

    def test_change_theme_background_status(self):
        self.browser.get(self.live_server_url)
        button = self.browser.find_element_by_id('button1')
        button.click()
        css = self.browser.find_element_by_id('body1')
        bg = css.value_of_css_property('background-color')
        self.assertEqual(bg, 'rgba(255, 239, 213, 1)')

    def test_change_theme_background_about(self):
        self.browser.get(self.live_server_url)
        button_about = self.browser.find_element_by_id('about')
        button_about.click()
        button = self.browser.find_element_by_id('button2')
        button.click()
        css = self.browser.find_element_by_id("body2")
        bg = css.value_of_css_property('background-color')
        self.assertEqual(bg, 'rgba(143, 188, 143, 1)')


    
