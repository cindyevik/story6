$(document).ready(function() {
    var clicked = false;
    $('.button2').click(function() {
        if (clicked) {
            $('body').css('background-color', '#FFEFD5');
            $('#row1').css('background', '#8FBC8F');
            $('.text2').css('color', '#FFEFD5')
            $('.button2').css('background-color', '#8FBC8F');
            $('.button2').css('border-color', '#8FBC8F');
            $('.button2').css('color', '#FFEFD5');
            $('.accordion').css('background-color', 'rgb(120, 172, 120)');
            $('.accordion').css('color', '#FFEFD5');
            $('.accordion:hover ').css('background-color', '#FFEFD5');
            $('.panel').css('background-color', 'rgba(143, 188, 143, 0.486)');
            $('.panel').css('color', '#FFEFD5');
            $('#header').css('background-color', '#8FBC8F');
            $('#header').css('color', '#FFEFD5');
            $('.btn').css('color', '#FFEFD5');
            $('.btn').css('background-color', '#8FBC8F');
            clicked = false;
        } else {
            $('body').css('background-color', '#8FBC8F');
            $('#row1').css('background', '#FFEFD5');
            $('.text2').css('color', '#8FBC8F')
            $('.button2').css('background-color', '#FFEFD5');
            $('.button2').css('border-color', '#FFEFD5');
            $('.button2').css('color', '#8FBC8F');
            $('.accordion').css('background-color', '#FFEFD5');
            $('.accordion').css('color', 'rgb(120, 172, 120)');
            $('.accordion:hover ').css('background-color', '#8FBC8F');
            $('.panel').css('background-color', '#FFEFD5');
            $('.panel').css('color', '#8FBC8F');
            $('#header').css('background-color', '#FFEFD5');
            $('#header').css('color', '#8FBC8F');
            $('.btn').css('color', '#8FBC8F');
            $('.btn').css('background-color', '#FFEFD5');
            clicked = true;
        };
    });
});

$('.search').click(function() {
    var keyword = $('#search').val();
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + keyword,
        dataType: 'json',
        success: function(result) {
            $("#book")[0].innerHTML = "";
            for (i = 0; i < result.items.length; i++) {
                $("#book").append("<tr><td>" + result.items[i].volumeInfo.title + "</td><td>" +
                    result.items[i].volumeInfo.authors + "</td><td>" +
                    result.items[i].volumeInfo.publishedDate + "</td><td><img src=" +
                    result.items[i].volumeInfo.imageLinks.thumbnail + "alt=''/></td></tr>");
            }
        }
    });
});

$(document).ready(function() {
    var keyword = "tes";
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + keyword,
        dataType: 'json',
        success: function(result) {
            $("#book")[0].innerHTML = "";
            for (i = 0; i < result.items.length; i++) {
                $("#book").append("<tr><td>" + result.items[i].volumeInfo.title + "</td><td>" +
                    result.items[i].volumeInfo.authors + "</td><td>" +
                    result.items[i].volumeInfo.publishedDate + "</td><td><img src=" +
                    result.items[i].volumeInfo.imageLinks.thumbnail + "alt=''/></td></tr>");
            }
        }
    });
});