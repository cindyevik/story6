$(document).ready(function() {
    var clicked = false;
    $('.button1').click(function() {
        if (clicked) {
            $('body').css('background-color', '#8FBC8F');
            $('#button2').css('color', '#8FBC8F');
            $('#button2').css('background-color', '#FFEFD5');
            $('#button2').css('border-color', '#FFEFD5');
            $('.output_text').css('background-color', '#FFEFD5');
            $('.output_text').css('border-color', '#FFEFD5');
            $('#welcome_text').css('color', '#FFEFD5');
            $('#row1').css('background', '#FFEFD5');
            $('.text2').css('color', '#8FBC8F')
            $('.button1').css('background-color', '#FFEFD5');
            $('.button1').css('border-color', '#FFEFD5');
            $('.button1').css('color', '#8FBC8F');
            clicked = false;
        } else {
            $('body').css('background-color', '#FFEFD5');
            $('#button2').css('color', '#FFEFD5');
            $('#button2').css('background-color', '#8FBC8F');
            $('#button2').css('border-color', '#8FBC8F');
            $('.output_text').css('background-color', '#8FBC8F');
            $('.output_text').css('border-color', '#8FBC8F');
            $('#welcome_text').css('color', '#8FBC8F');
            $('#row1').css('background', '#8FBC8F');
            $('.text2').css('color', '#FFEFD5')
            $('.button1').css('background-color', '#8FBC8F');
            $('.button1').css('border-color', '#8FBC8F');
            $('.button1').css('color', '#FFEFD5');
            clicked = true;
        };
    });
});