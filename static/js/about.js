$('.accordion').click(function() {
    var $this = $(this);
    if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(400);
    }
    $this.next().slideToggle(400);
});

$(document).ready(function() {
    var clicked = false;
    $('.button2').click(function() {
        if (clicked) {
            $('body').css('background-color', '#FFEFD5');
            $('#row1').css('background', '#8FBC8F');
            $('.text2').css('color', '#FFEFD5')
            $('.button2').css('background-color', '#8FBC8F');
            $('.button2').css('border-color', '#8FBC8F');
            $('.button2').css('color', '#FFEFD5');
            $('.accordion').css('background-color', 'rgb(120, 172, 120)');
            $('.accordion').css('color', '#FFEFD5');
            $('.accordion:hover ').css('background-color', '#FFEFD5');
            $('.panel').css('background-color', 'rgba(143, 188, 143, 0.486)');
            $('.panel').css('color', '#FFEFD5');
            clicked = false;
        } else {
            $('body').css('background-color', '#8FBC8F');
            $('#row1').css('background', '#FFEFD5');
            $('.text2').css('color', '#8FBC8F')
            $('.button2').css('background-color', '#FFEFD5');
            $('.button2').css('border-color', '#FFEFD5');
            $('.button2').css('color', '#8FBC8F');
            $('.accordion').css('background-color', '#FFEFD5');
            $('.accordion').css('color', 'rgb(120, 172, 120)');
            $('.accordion:hover ').css('background-color', '#8FBC8F');
            $('.panel').css('background-color', '#FFEFD5');
            $('.panel').css('color', '#8FBC8F');
            clicked = true;
        };
    });
});